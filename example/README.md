# How to use IntelliFL #

There are five example projects in the directory of "example": [Joda-Time](http://www.joda.org/joda-time/) (Time), 
[Apache commons-lang](https://commons.apache.org/proper/commons-lang/) (Lang), [JFreechart](http://www.jfree.org/jfreechart/) (Chart), 
[Apache commons-math](http://commons.apache.org/proper/commons-math/) (Math), Google [Closure-compiler](https://github.com/google/closure-compiler) (Closure).
They are all from [Defects4j](https://github.com/rjust/defects4j), which is an open source repository, providing 
some buggy versions and corresponding fixed versions of different projects.
So we can get the buggy methods by using "diff" command for buggy and fixed versions.  

We take Joda-Time as an example to illustrate how to use IntelliFL. Its buggy method is 

   ```org.joda.time.field.UnsupportedDurationField:compareTo(Lorg/joda/time/DurationField;)I```

1. Enter the directory of example/Joda-Time, and run 
```mvn clean test``` to compile it. 

2. There is one failed test after compilation:
 `testWith_baseAndArgHaveNoRange(org.joda.time.TestPartial_Basics)`

3. Run IntelliFL as follows:

       ```mvn org.intelliFL:intelliFL-maven-plugin:1.0-SNAPSHOT:prfl-comb```

4. Some suspicious methods and their values are printed in screen. The full list of methods are
   stored in intelliFL/outputDir/pr_ranking/PR_Ochiai.csv. For example, the top three suspicious methods are
   
       ```org/joda/time/field/UnsupportedDurationField:compareTo(Lorg/joda/time/DurationField;)I 0.280008077032```

       ```org/joda/time/Partial:<init>(Lorg/joda/time/DateTimeFieldType;ILorg/joda/time/Chronology;)V 0.167915638108```

       ```org/joda/time/Partial:<init>(Lorg/joda/time/DateTimeFieldType;I)V 0.163945151214```
  
5. Then, we can check each method if there are bugs inside it according to this ranking list. In this example, IntelliFL 
   rank the suspicious method "compareTo" as first one among 3000+ source code methods successfully.

The following table shows required JDK version and buggy methods of the five projects. Failed tests and final ranking list can be printed. 

| Project | JDK | Buggy Method |
|---|---|---|
| Time |7| org.joda.time.field.UnsupportedDurationField:compareTo(Lorg/joda/time/DurationField;)I |
| Lang |7| org.apache.commons.lang3.math.NumberUtils:createNumber(Ljava/lang/String;)Ljava/lang/Number; |  
| Chart |7/8/9 |org.jfree.chart.renderer.GrayPaintScale:getPaint(D)Ljava/awt/Paint;| 
| Math|7/8/9|org.apache.commons.math.fraction.Fraction:compareTo(Lorg/apache/commons/math/fraction/Fraction;)I|
|Closure|7|com.google.javascript.jscomp.FlowSensitiveInlineVariables$1:apply(Lcom/google/javascript/rhino/Node;)Z|
