/**
 * The MIT License
 * Copyright © 2017 The intelliFL team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.intelliFL.cg;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jgrapht.DirectedGraph;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.traverse.BreadthFirstIterator;

import set.intelliFL.log.IntelliFLLogger;

public class CallGraph
{
	// store the reverse call graph
	static DirectedGraph<String, DefaultEdge> reverseCG = new DefaultDirectedGraph<String, DefaultEdge>(
			DefaultEdge.class);

	public static void printStatistics() {
		//IntelliFLLogger.info("===============================================");
		IntelliFLLogger.info("Call Graph statistics:");
		IntelliFLLogger.info("Node number: " + reverseCG.vertexSet().size());
		IntelliFLLogger.info("Edge number: " + reverseCG.edgeSet().size());
	}

	public static void printCG(PrintStream printer) {
		/*for (DefaultEdge edge : reverseCG.edgeSet()) {
			printer.println(Data.getDecoding(reverseCG.getEdgeTarget(edge))
					+ "=>" + Data.getDecoding(reverseCG.getEdgeSource(edge)));
		}*/
		HashMap<String,ArrayList<String>> callerAndCallee=new HashMap<String,ArrayList<String>>();
		for (DefaultEdge edge : reverseCG.edgeSet()) {
			String caller=Data.getDecoding(reverseCG.getEdgeTarget(edge));
			String callee=Data.getDecoding(reverseCG.getEdgeSource(edge));
			if(!callerAndCallee.containsKey(caller)){
				ArrayList<String> callees=new ArrayList<String>();
				callees.add(callee);
				callerAndCallee.put(caller, callees);
			}else
				callerAndCallee.get(caller).add(callee);
		}
		for (String caller:callerAndCallee.keySet()){
			printer.print(caller+"==>");
			for(String callee:callerAndCallee.get(caller)){
				printer.print(callee+" ");
			}
			printer.print("\n");
			
		}
		
	}

	// get transive caller using BFS
	public static Set<String> getTransitiveCallee(String node) {
		Set<String> res = new HashSet<String>();
		List<String> workset = new ArrayList<String>();
		workset.add(node);
		BreadthFirstIterator traverser = new BreadthFirstIterator(reverseCG,
				node);
		while (traverser.hasNext()) {
			res.add((String) traverser.next());
		}
		// level-1 caller, debugging purpose
		// Set<DefaultEdge> edges=reverseCG.outgoingEdgesOf(node);
		// for(DefaultEdge edge:edges){
		// res.add(reverseCG.getEdgeTarget(edge));
		// }

		return res;
	}

	public static void printPath(String file, String source, String target)
			throws IOException {
		List<DefaultEdge> path = DijkstraShortestPath.findPathBetween(reverseCG,
				source, target);
		BufferedWriter writer = new BufferedWriter(
				new FileWriter(file + ".txt"));
		for (DefaultEdge edge : path) {
			writer.write(Data.getDecoding(reverseCG.getEdgeSource(edge)) + "=>"
					+ Data.getDecoding(reverseCG.getEdgeTarget(edge)) + "||||"
					+ reverseCG.getEdgeSource(edge) + "=>"
					+ reverseCG.getEdgeTarget(edge) + "\n");
		}
		writer.flush();
		writer.close();
	}
}
