/**
 * The MIT License
 * Copyright © 2017 The intelliFL team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.intelliFL.maven;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public class SurefireClassVisitor extends ClassVisitor implements Opcodes
{
	public SurefireClassVisitor(ClassVisitor cv) {
		super(ASM5, cv);
	}

	@Override
	public MethodVisitor visitMethod(final int access, final String name,
			final String desc, final String signature,
			final String[] exceptions) {
		MethodVisitor mv = this.cv.visitMethod(access, name, desc, signature,
				exceptions);
		if ((name.equals("execute")) && (desc.equals("()V"))) {
			mv = new SurefireMethodVisitor(mv, name, desc);
		}
		return mv;
	}
}

class SurefireMethodVisitor extends MethodVisitor implements Opcodes
{
	String methName;
	String desc;

	public SurefireMethodVisitor(MethodVisitor mv, String name, String desc) {
		super(ASM5, mv);
		this.methName = name;
		this.desc = desc;
	}

	@Override
	public void visitCode() {
		mv.visitVarInsn(ALOAD, 0);
		mv.visitMethodInsn(INVOKESTATIC, SurefireRewriter.className, methName,
				desc.replace("(", "(Ljava/lang/Object;"), false);
		// mv.visitInsn();
		mv.visitCode();
	}
}
