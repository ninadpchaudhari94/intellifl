/**
 * The MIT License
 * Copyright © 2017 The intelliFL team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.intelliFL.maven;

import java.io.File;
import java.lang.management.ManagementFactory;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;


import set.intelliFL.log.IntelliFLLogger;

/**
 * 
 * This class was largely copied from Ekstazi
 *
 */
public class DynamicAgentLoader
{
	private static final String TOOLS_JAR_NAME = "tools.jar";
	private static final String CLASSES_JAR_NAME = "classes.jar";
	private static final String LOADED = DynamicAgentLoader.class.getName()
			+ " Loaded";

	public static boolean loadDynamicAgent(String jarURL, String arguments) {
		try {
			if (System.getProperty(LOADED) != null) {
				return true;
			}
			System.setProperty(LOADED, "");
			return loadAgent(jarURL, arguments);
		} catch (Exception ex) {
			if (System.getProperty("java.version").startsWith("9"))
				throw new RuntimeException(
						"Agent loading failure for JDK9! Don't forget to set jdk.attach.allowAttachSelf, e.g., export MAVEN_OPTS=\"-Djdk.attach.allowAttachSelf=true\".");
			else
				throw new RuntimeException("Agent loading failure!");
		}
	}

	public static boolean loadAgent(String aju, String arguments)
			throws Exception {
		URL toolsJarFile = findToolsJar();
		if (toolsJarFile == null) {
			return false;
		}
		IntelliFLLogger.info(toolsJarFile.toString());

		Class<?> vc = loadVirtualMachine(new URL[] { toolsJarFile });
		if (vc == null) {
			return false;
		}

		attachAgent(vc, aju, arguments);
		return true;
	}

	private static void attachAgent(Class<?> vc, String aju, String arguments)
			throws Exception {
		String pid = getPID();
		Object vm = getAttachMethod(vc).invoke(null, new Object[] { pid });
		getLoadAgentMethod(vc).invoke(vm, new Object[] { aju, arguments });
		getDetachMethod(vc).invoke(vm);
	}

	private static Method getLoadAgentMethod(Class<?> vc)
			throws SecurityException, NoSuchMethodException {
		return vc.getMethod("loadAgent",
				new Class[] { String.class, String.class });
	}

	private static Method getAttachMethod(Class<?> vc)
			throws SecurityException, NoSuchMethodException {
		return vc.getMethod("attach", new Class<?>[] { String.class });
	}

	private static Method getDetachMethod(Class<?> vc)
			throws SecurityException, NoSuchMethodException {
		return vc.getMethod("detach");
	}

	private static Class<?> loadVirtualMachine(URL[] urls) throws Exception {
		URLClassLoader loader = new URLClassLoader(urls,
				ClassLoader.getSystemClassLoader());
		return loader.loadClass("com.sun.tools.attach.VirtualMachine");
	}

	private static String getPID() {
		String vmName = ManagementFactory.getRuntimeMXBean().getName();
		return vmName.substring(0, vmName.indexOf("@"));
	}

	private static URL findToolsJar() throws MalformedURLException {
		String javaHome = System.getProperty("java.home");
		File javaHomeFile = new File(javaHome);
		File tjf = new File(javaHomeFile,
				"lib" + File.separator + TOOLS_JAR_NAME);

		if (!tjf.exists()) {
			tjf = new File(System.getenv("java_home"),
					"lib" + File.separator + TOOLS_JAR_NAME);
		}

		if (!tjf.exists() && javaHomeFile.getAbsolutePath()
				.endsWith(File.separator + "jre")) {
			javaHomeFile = javaHomeFile.getParentFile();
			tjf = new File(javaHomeFile,
					"lib" + File.separator + TOOLS_JAR_NAME);
		}

		if (!tjf.exists() && isMac() && javaHomeFile.getAbsolutePath()
				.endsWith(File.separator + "Home")) {
			javaHomeFile = javaHomeFile.getParentFile();
			tjf = new File(javaHomeFile,
					"Classes" + File.separator + CLASSES_JAR_NAME);
		}

		return tjf.toURI().toURL();
	}

	private static boolean isMac() {
		return System.getProperty("os.name").toLowerCase().indexOf("mac") >= 0;
	}

}
