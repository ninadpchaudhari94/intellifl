/**
 * The MIT License
 * Copyright © 2017 The intelliFL team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.intelliFL.metrics;

public class MetricData
{
	public int numFrame = 0;
	public int numInsn = 0;
	public int numIntInsn = 0;
	public int numVarInsn = 0;
	public int numTypeInsn = 0;
	public int numFieldInsn = 0;
	public int numMethInsn = 0;
	public int numInvokeDynamicInsn = 0;
	public int numJumpInsn = 0;
	public int numLabelInsn = 0;
	public int numLdcInsn = 0;
	public int numIincInsn = 0;
	public int numTableSwitchInsn = 0;
	public int numLookupSwitchInsn = 0;
	public int numMultiANewArrayInsn = 0;
	public int numTryCatchBlock = 0;

	public static final String sep = " ";

	public String toString() {
		return numFrame + sep + numInsn + sep + numIntInsn + sep + numVarInsn
				+ sep + numTypeInsn + sep + numFieldInsn + sep + numMethInsn
				+ sep + numInvokeDynamicInsn + sep + numJumpInsn + sep
				+ numLabelInsn + sep + numLdcInsn + sep + numIincInsn + sep
				+ numTableSwitchInsn + sep + numLookupSwitchInsn + sep
				+ numMultiANewArrayInsn + sep + numTryCatchBlock;
	}
}
