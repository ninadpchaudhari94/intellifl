/**
 * The MIT License
 * Copyright © 2017 The intelliFL team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.intelliFL;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.surefire.util.DirectoryScanner;
import org.apache.maven.plugins.annotations.Execute;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.surefire.booter.Classpath;
import org.apache.maven.surefire.testset.TestListResolver;
import org.apache.maven.surefire.util.DefaultScanResult;

import set.intelliFL.cg.CallGraphBuilder;
import set.intelliFL.metrics.MetricAnalyzer;

/**
 * This mojo will create the code.metrics file under intelliFL/intelliFL-metrics
 * directory. Each line in the file include the source code method name with the
 * 16 bytecode metrics (used in our icse18 submission)
 * @author lingmingzhang
 *
 */
@Mojo(name = "metrics", requiresDependencyResolution = ResolutionScope.TEST)
@Execute(phase = LifecyclePhase.TEST_COMPILE)
public class MetricMojo extends BaseMojo
{

	public void execute() throws MojoExecutionException {
		List<String> classes = getAllSrcClasses();
		Classpath cp = getSurefireClasspath();
		ClassLoader loader = createClassLoader(cp);

		List<String> classFiles = new ArrayList<String>();
		for (String clazz : classes) {
			URL url = loader.getResource(getSlashName(clazz));
			classFiles.add(url.getFile());
		}
		Properties.BASE_DIR = baseDir.toString();
		MetricAnalyzer.analyze(classFiles);

	}

	protected List<String> getAllClasses() {
		DirectoryScanner testScanner = new DirectoryScanner(
				getTestClassesDirectory(), new TestListResolver("*"));
		DirectoryScanner classScanner = new DirectoryScanner(
				getClassesDirectory(), new TestListResolver("*"));
		DefaultScanResult scanResult = classScanner.scan()
				.append(testScanner.scan());
		return scanResult.getFiles();
	}

	protected List<String> getAllSrcClasses() {
		DirectoryScanner classScanner = new DirectoryScanner(
				getClassesDirectory(), new TestListResolver("*"));
		DefaultScanResult scanResult = classScanner.scan();
		return scanResult.getFiles();
	}

}
