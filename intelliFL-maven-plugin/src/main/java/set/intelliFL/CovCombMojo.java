package set.intelliFL;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Execute;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;

import set.intelliFL.log.IntelliFLLogger;

/**
 * This mojo will create coverage files of statement or method level under
 * intelliFL/intelliFL-stmt-cov or intelliFL/intelliFL-meth-cov directory, 
 * depending on the parameter. 
 * Each file record the coverage for a test method, with the first line 
 * indicating the test name and test outcome ("true"
 * denotes passing, "false" denotes failing). All the following lines represent
 * the covered statements or methods
 * 
 * @author lingmingzhang
 *
 */
@Mojo(name = "comb-cov", requiresDependencyResolution = ResolutionScope.TEST)
@Execute(phase = LifecyclePhase.TEST_COMPILE)
public class CovCombMojo extends BaseCovMojo{
	public void execute() throws MojoExecutionException {
		if(aggregation==true){
			IntelliFLLogger.info("Starting statement coverage analysis...");
			this.coverageLevel="stmt-cov";
			super.execute();
			IntelliFLLogger.info("Statement coverage analysis DONE");
		}else{
			IntelliFLLogger.info("Starting method coverage analysis...");
			this.coverageLevel="meth-cov";
			super.execute();
			IntelliFLLogger.info("Method coverage analysis DONE");
		}
	}
}
